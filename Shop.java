import java.util.Scanner;

public class Shop {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		Food[] fd = new Food[4];
		Food f = new Food();
		
		for(int i=0; i <=3; i++) {
			fd[i]= new Food();
			
			System.out.println("Food: ");
			fd[i].name = sc.next();
			System.out.println("Calories: ");
			fd[i].calories= sc.nextDouble();
			System.out.println("It is on sale? ");
			fd[i].isOnSale = sc.nextBoolean();
		}
		f.minus(fd[3].calories);
		
		System.out.println("The Food is "+fd[3].name+" and it is "+fd[3].calories+" calories.");
		if(fd[3].isOnSale == true) {
			System.out.println("It is on sale.");
		}
		else {
			System.out.println("It is not on sale.");
		}
	}

}



/*fd[i].calories= Integer.parseInt(sc.nextLine())*/